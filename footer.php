<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after
 *
 * @package UnderStrap
 */

// Exit if accessed directly.
defined( 'ABSPATH' ) || exit;
/*
$container = get_theme_mod( 'understrap_container_type' );
?>

<?php get_template_part( 'sidebar-templates/sidebar', 'footerfull' ); ?>

<div class="wrapper" id="wrapper-footer">

	<div class="<?php echo esc_attr( $container ); ?>">

		<div class="row">

			<div class="col-md-12">

				<footer class="site-footer" id="colophon">

					<div class="site-info">

						<?php understrap_site_info(); ?>

					</div><!-- .site-info -->

				</footer><!-- #colophon -->

			</div><!--col end -->

		</div><!-- row end -->

	</div><!-- container end -->

</div><!-- wrapper end -->

</div><!-- #page we need this extra closing tag here -->

<?php */
?>

<!--AROUND FOOTER START HERE-->

<div class="container-fluid m-0 px-5" id="footer">

<div class="row">

<div class="col-3 info-col">
<h5>
<?php echo get_theme_mod( 'info_textarea_field' , _('')); ?> 
</h5>
</div>
<div class="col-6 scrolling-col">

<div class="m-scroll">
  <div class="m-scroll__title">
    <div>

	<?php 
	$scroll_txt = get_theme_mod( 'scroll_textarea_field' , _(''));
	$txt_arr = explode(' ',$scroll_txt);
	foreach($txt_arr as $t):
	?>

	<h4> <span class="hover_stroke"> <?php echo $t?> &nbsp; </span></h4>

	<?php endforeach; ?>
    </div>
  </div>
</div>

</div>
<div class="col-3 social-col">

<a href="<?php echo get_theme_mod( 'about_fb_link' , _('#'));?>"> <i class="social-icon fa fa-facebook-square ml-5"></i> </a>
<a href="<?php echo get_theme_mod( 'about_ig_link' , _('#'));?>"> <i class="social-icon fa fa-instagram  ml-5"></i> </a>

</div>

</div>

</div>

<?php wp_footer(); ?>

<script>
  $( function() {
    $(".draggable" ).draggable();
  } );
  </script>

</body>

</html>

