<?php 

$draggable = get_field('draggable_images') ?: false;

console_log($draggable);


if($draggable):
    $draggable = $draggable[0];
    $images = $draggable['images'];
    $desc = $draggable['text_block'];
    $position = $draggable['position'] == true ? 'right' : 'left';

    //console_log($images);
    // console_log($desc);
    // console_log($position)
?>

<!-- draggable box with or without descriptio -->

<div class="row" style="position:relative">

<!--images col-->

<div class="col-12 col-md-6 drag_images_container">

    <?php foreach($images as $img): 
        $image = $img['image'];
        $margin_arr = ['bottom','top','left','right'];
        $direction = rand(0,3);
        $margin = $margin_arr[$direction];
        $size = rand(0,150);
        $size_to_str = strval($size);
        // console_log($size_to_str);
        $px = $size_to_str . 'px';
        // console_log($margin);

        // console_log($image);
        ?>
        
        <div class="draggable drag_bg" style='background-image:url("<?php echo $image ?>"); <?php echo $margin ?>:-<?php echo $px ?> ; z-index:9'></div>

    <?php
    endforeach; ?>

</div>

<div class="col-12 col-md-6">
    <div class="drag_descrition">
        <?php echo $desc ?>
    </div>
</div>

<hr style="width: 100%;margin: 2em 0;">

</div>


<?php 
else:
?>

<div class="none">this div will never appear apparentlye</div>

<?php 
endif;
?>

<style>
    .drag_bg{
        background-size:contain;
    }
</style>
