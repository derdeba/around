<?php
/*
Template Name: Comingsoon
Template Post Type: post, page, event
*/


?>

<!DOCTYPE html>
<html>
  <head>
    <script src="<?php echo get_stylesheet_directory_uri() . '/cooming-soon/blotter.min.js'?>"></script>
    <script src="<?php echo get_stylesheet_directory_uri() . '/cooming-soon/materials/liquidDistortMaterial.js'?>"></script>
    <script
      src="https://code.jquery.com/jquery-3.5.1.min.js"
      integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0="
      crossorigin="anonymous"
    ></script>
    <link rel="stylesheet" href="<?php echo get_stylesheet_directory_uri() . '/css/experimental.css'?>">
    <link rel="preconnect" href="https://fonts.googleapis.com">
<link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
<link href="https://fonts.googleapis.com/css2?family=Roboto+Slab:wght@800&display=swap" rel="stylesheet">
  </head>
  <body>

    <div id="text-container">
      <p class="left">/ Cooming Soon - 01/01/2022 00:00 UTM zone 18N /</p>
        <p class="right">/ &copy; Around Eyewear /</p>
    </div>

    <script>
        var text = new Blotter.Text("Around", {
          family: 'Roboto',
          size: 800,
          weight:300,
          fill: "black",
        });

        var material = new Blotter.LiquidDistortMaterial();
        material.uniforms.uSpeed.value = 0.1;
        material.uniforms.uVolatility.value = 0.51;

        var blotter = new Blotter(material, { texts: text });

        var scope = blotter.forText(text);
        var test = scope.context;
        console.log(test);
        el = $("#text-container");
        scope.appendTo(el);

      const textPath = document.querySelector("#text-path");

      const h = document.documentElement,
        b = document.body,
        st = "scrollTop",
        sh = "scrollHeight";

      document.addEventListener("scroll", (e) => {
        let percent =
          ((h[st] || b[st]) / ((h[sh] || b[sh]) - h.clientHeight)) * 100;
        textPath.setAttribute("startOffset", -percent * 40 + 1200);
      });
    </script>
  </body>
</html>
