<?php
/**
 * The header for our theme
 *
 * Displays all of the <head> section and everything up till <div id="content">
 *
 * @package UnderStrap
 */

// Exit if accessed directly.
defined( 'ABSPATH' ) || exit;

$container = get_theme_mod( 'understrap_container_type' );
?>
<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?> <?php understrap_body_attributes(); ?>>
<?php do_action( 'wp_body_open' ); ?>
<div class="site" id="page">

<!--logo bar-->

<div class="menu_bar <?php if(is_home()){echo 'menu_bar_resize';}else{'';}?>">

	<div class="logo">

	<?php the_custom_logo(); ?>

	</div>

	<?php echo rarus_polylang_languages() ?>

</div>	

<!--menu bar-->

<div id="menu_list" class="<?php if(is_home()){echo 'opened';}else{'';}?>">
		
	<!-- The WordPress Menu goes here -->
	<?php
				wp_nav_menu(
					array(
						'theme_location'  => 'primary',
						'container_class' => 'menu_container',
						'container_id'    => 'around_menu',
						'menu_class'      => 'around-nav',
						'fallback_cb'     => '',
						'menu_id'         => 'main-menu',
						'link_before' => '<div class="menu_txt">' , 
						'link_after' => '</div>',
						'depth'           => 2,
						'walker'          => new Understrap_WP_Bootstrap_Navwalker(),
					)
				);
				?>

<!--footer menu-->
<div class="container-fluid mt-3 px-5" id="footer-menu">
<div class="row">
<div class="col-6 about-txt-col">

<h5 class="italic colotext">

<?php 

//get theme cutomizer options 
echo get_theme_mod( 'about_textarea_field' , _('Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed')); 
?>

</h5>

</div>

<div class="col-6 social-col">

<a href="<?php echo get_theme_mod( 'about_fb_link' , _('#'));?>"> <i class="social-icon fa fa-facebook-square ml-5"></i> </a>
<a href="<?php echo get_theme_mod( 'about_ig_link' , _('#'));?>"> <i class="social-icon fa fa-instagram  ml-5"></i> </a>

</div>


</div>

</div>


<div class="redband">

<img src="<?php echo get_stylesheet_directory_uri().'/assets/triangolo.svg' ?>" alt="" srcset="" class="triangle <?php if(is_home()){echo 'opened';}else{'';}?>">	

</div>

</div>

</div><!-- #wrapper-navbar end -->
