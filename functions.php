<?php
/**
 * UnderStrap functions and definitions
 *
 * @package UnderStrap
 */

// Exit if accessed directly.
defined( 'ABSPATH' ) || exit;

// UnderStrap's includes directory.
$understrap_inc_dir = get_template_directory() . '/inc';

// Array of files to include.
$understrap_includes = array(
	'/theme-settings.php',                  // Initialize theme default settings.
	'/setup.php',                           // Theme setup and custom theme supports.
	'/widgets.php',                         // Register widget area.
	'/enqueue.php',                         // Enqueue scripts and styles.
	'/template-tags.php',                   // Custom template tags for this theme.
	'/pagination.php',                      // Custom pagination for this theme.
	'/hooks.php',                           // Custom hooks.
	'/extras.php',                          // Custom functions that act independently of the theme templates.
	'/customizer.php',                      // Customizer additions.
	'/custom-comments.php',                 // Custom Comments file.
	'/class-wp-bootstrap-navwalker.php',    // Load custom WordPress nav walker. Trying to get deeper navigation? Check out: https://github.com/understrap/understrap/issues/567.
	'/editor.php',                          // Load Editor functions.
	'/deprecated.php',                      // Load deprecated functions.
);

// Load WooCommerce functions if WooCommerce is activated.
if ( class_exists( 'WooCommerce' ) ) {
	$understrap_includes[] = '/woocommerce.php';
}

// Load Jetpack compatibility file if Jetpack is activiated.
if ( class_exists( 'Jetpack' ) ) {
	$understrap_includes[] = '/jetpack.php';
}

// Include files.
foreach ( $understrap_includes as $file ) {
	require_once $understrap_inc_dir . $file;
}


// CUSTOM AROUND EYEWEAR CODE


//PHP CONSOLE LOG -- ONLY ON DEV

function console_log($output, $with_script_tags = true) {

	$issviluppo = false;
	if (isset($_SERVER['SERVER_NAME'])) {
		$sn = $_SERVER['SERVER_NAME'];
		$issviluppo = (preg_match("/^(127\.|localhost|zetta|zetta|localhost:3000\.local)$/",$sn)>0);
	}

	if($issviluppo){

    $js_code = 'console.log(' . json_encode($output, JSON_HEX_TAG) . 
	');';
    if ($with_script_tags) {
        $js_code = '<script>' . $js_code . '</script>';
    }
    echo $js_code;
	}
	else{return;}

}

// ADD CLASS TO GET_LOGO

add_filter( 'get_custom_logo', 'change_logo_class' );


function change_logo_class( $html ) {

    $html = str_replace( 'img-fluid', 'img-fluid around-logo', $html );
    $html = str_replace( 'navbar-brand', 'around-logo-link', $html );

    return $html;
}


//POLYLANG CUSTOM LANGUAGE SWITCHER

/**
 * Show Polylang Languages with Custom Markup
 * @param  string $class Add custom class to the languages container
 * @return string        
 */
function rarus_polylang_languages( $class = '' ) {

	if ( ! function_exists( 'pll_the_languages' ) ) return;
  
	// Gets the pll_the_languages() raw code
	$languages = pll_the_languages( array(
	  'display_names_as'       => 'slug',
	  'hide_if_no_translation' => 1,
	  'raw'                    => true
	) ); 
  
	$output = '';
  
	// Checks if the $languages is not empty
	if ( ! empty( $languages ) ) {
  
	  // Creates the $output variable with languages container
	  $output = '<div class="languages' . ( $class ? ' ' . $class : '' ) . '">';
  
	  // Runs the loop through all languages
	  foreach ( $languages as $language ) {
  
		// Variables containing language data
		$id             = $language['id'];
		$slug           = $language['slug'];
		$url            = $language['url'];
		$current        = $language['current_lang'] ? ' languages__item--current' : '';
		$no_translation = $language['no_translation'];
		
		console_log($language);

		// Checks if the page has translation in this language
		if ( ! $no_translation ) {
		  // Check if it's current language
		  if ( $current ) {
			// Output the language in a <span> tag so it's not clickable
			$output .= "<span class=\"languages__item$current\">$slug</span>";
		  } else {
			// Output the language in an anchor tag
			$output .= "<a href=\"$url\" class=\"languages__item$current\">$slug</a>";
		  }
		}
  
	  }
  
	  $output .= '</div>';
  
	}
  
	return $output;
  }

  /* Creating Editor Blocks with ACF PRO */

function around_acf_blocks_init() {

    // Check if ACF PRO function exists.
    if( function_exists('acf_register_block_type') ) :

        // Register blocks. Don't edit anything between the following comments.
        /* Pinegrow generated Register Blocks Begin */

        acf_register_block_type( array(
            'name' => 'drag_box',
            'title' => __( 'Drag Box', 'around' ),
            'icon' => 'book-alt',
            'category' => 'common',
            'enqueue_style' => get_template_directory_uri() . '/css/drag_box.css',
            'render_template' => 'template-parts/blocks/drag_box.php'
        ) );

    /* Pinegrow generated Register Blocks End */
    endif;
}
add_action('acf/init', 'around_acf_blocks_init');

/* End of creating Editor Blocks with ACF PRO */


/**
 * Register customizer section
 */

add_action( 'customize_register', function( $wp_customize ) {

// Theme Options Panel
$wp_customize->add_panel( 'about_panel', 
    array(
        //'priority'       => 100,
        'title'            => __( 'About Section', 'around' ),
        'description'      => __( 'Theme Modifications like color scheme, theme texts and layout preferences can be done here', 'nd_dosth' ),
    ) 
);

// Text Options Section Inside Theme
$wp_customize->add_section( 'about_section', 
    array(
        'title'         => __( 'About Section', 'around' ),
        'priority'      => 1,
        'panel'         => 'about_panel'
    ) 
);

// Text Options Section Inside Theme
$wp_customize->add_section( 'social_section', 
    array(
        'title'         => __( 'Social Links', 'around' ),
        'priority'      => 2,
        'panel'         => 'about_panel'
    ) 
);

// Setting for About text.
$wp_customize->add_setting( 'about_textarea_field',
    array(
        'default'           => __( 'lorem ipsum ', 'around' ),
        'sanitize_callback' => 'sanitize_text_field',
        'transport'         => 'refresh',
		'type' => 'theme_mod',
    )
);

// Control for About text
$wp_customize->add_control( 'about_textarea_field', 
    array(
        'type'        => 'textarea',
        'priority'    => 10,
        'section'     => 'about_section',
        'label'       => 'About description',
        'description' => 'Tell me something about your project',
    ) 
);

// Setting for FB link.
$wp_customize->add_setting( 'about_fb_link',
    array(
        'default'           => __( '', 'around' ),
        'sanitize_callback' => 'sanitize_text_field',
        'transport'         => 'refresh',
		'type' => 'theme_mod',
    )
);

// Control for FB link
$wp_customize->add_control( 'about_fb_field', 
    array(
		'settings' => 'about_fb_link',
        'type'        => 'url',
        'priority'    => 11,
        'section'     => 'social_section',
        'label'       => 'Facebook',
        'description' => 'Insert facebook link',
		'input_attrs' => array(
			'placeholder' => __( 'https://www.facebook.com' ),
		  ),
    ) 
);

// Setting for IG link.
$wp_customize->add_setting( 'about_ig_link',
    array(
        'default'           => __( '', 'around' ),
        'sanitize_callback' => 'sanitize_text_field',
        'transport'         => 'refresh',
		'type' => 'theme_mod',
    )
);

// Control for IG link
$wp_customize->add_control( 'about_ig_field', 
    array(
		'settings' => 'about_ig_link',
        'type'        => 'url',
        'priority'    => 11,
        'section'     => 'social_section',
        'label'       => 'Instagram',
        'description' => 'Insert facebook link',
		'input_attrs' => array(
			'placeholder' => __( 'https://www.instagram.com' ),
		  ),
    ) 
);

//FOOTER PANEL

// Theme Options Panel
$wp_customize->add_panel( 'footer_panel', 
    array(
        //'priority'       => 100,
        'title'            => __( 'Footer', 'around' ),
        'description'      => __( 'Footer stuff', 'nd_dosth' ),
    ) 
);

// Text Options Section Inside Theme
$wp_customize->add_section( 'footer_section', 
    array(
        'title'         => __( 'Footer', 'around' ),
        'priority'      => 1,
        'panel'         => 'footer_panel'
    ) 
);

// Text Options Section Inside Theme
$wp_customize->add_section( 'info_section', 
    array(
        'title'         => __( 'Info', 'around' ),
        'priority'      => 2,
        'panel'         => 'footer_panel'
    ) 
);

// Setting for About text.
$wp_customize->add_setting( 'info_textarea_field',
    array(
        'default'           => __( '
        AROUND eyewear
        eyeawear str. 20100 MI Italy
        90000090', 'around' ),
        'sanitize_callback' => 'sanitize_text_field',
        'transport'         => 'refresh',
		'type' => 'theme_mod',
    )
);

// Control for About text
$wp_customize->add_control( 'info_textarea_field', 
    array(
        'type'        => 'textarea',
        'priority'    => 10,
        'section'     => 'info_section',
        'label'       => 'Info',
        'description' => 'add agency info here',
    ) 
);

// Text Options Section Inside Theme
$wp_customize->add_section( 'scroll_section', 
    array(
        'title'         => __( 'Scrolling text', 'around' ),
        'priority'      => 2,
        'panel'         => 'footer_panel'
    ) 
);

$wp_customize->add_setting( 'scroll_textarea_field',
    array(
        'default'           => __( 'BOTTOM TEXT' ),
        'sanitize_callback' => 'sanitize_text_field',
        'transport'         => 'refresh',
		'type' => 'theme_mod',
    )
);

// Control for About text
$wp_customize->add_control( 'scroll_textarea_field', 
    array(
        'type'        => 'textarea',
        'priority'    => 10,
        'section'     => 'scroll_section',
        'label'       => 'Info',
        'description' => 'add agency info here',
    ) 
);

}, 100 );

//woocommerce custom archive


// After setup theme hook adds WC support
function around_add_woocommerce_support() {
    add_theme_support( 'woocommerce' ); // <<<< here
}
add_action( 'after_setup_theme', 'around_add_woocommerce_support' );

//hide title and stuff

// add_filter( 'woocommerce_show_page_title', '__return_null' );
remove_action( 'woocommerce_before_shop_loop', 'woocommerce_catalog_ordering', 30 );
remove_action( 'woocommerce_before_shop_loop', 'woocommerce_result_count', 20 );
remove_action( 'woocommerce_before_main_content', 'woocommerce_breadcrumb', 20, 0);
remove_action( 'woocommerce_after_shop_loop_item_title', 'woocommerce_template_loop_price', 10 );

add_action('woocommerce_before_main_content','woocommerce_catalog_ordering', 11);

//Remove WooCommerce Tabs - this code removes all 3 tabs - to be more specific just remove actual unset lines 

add_filter( 'woocommerce_product_tabs', 'woo_remove_product_tabs', 98 );

function woo_remove_product_tabs( $tabs ) {

    unset( $tabs['description'] );      	// Remove the description tab
    unset( $tabs['reviews'] ); 			// Remove the reviews tab
    unset( $tabs['additional_information'] );  	// Remove the additional information tab

    return $tabs;

}

//redirect to coming soon

add_action( 'template_redirect', 'themeprefix_coming_soon' );
function themeprefix_coming_soon() {
	if( !is_user_logged_in() && ! is_front_page() || is_home() ){
		 wp_redirect( site_url() );
		 exit();
	}
}